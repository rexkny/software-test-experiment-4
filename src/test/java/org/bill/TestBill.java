package org.bill;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.text.ParseException;
import java.time.LocalDateTime;

public class TestBill {
    @DisplayName(value = "等价类测试用例")
    @ParameterizedTest
    @CsvFileSource(resources = "/等价类测试用例.csv", numLinesToSkip = 1, encoding = "UTF-8")
    void fileTest3(int m, int year, int month, int day,
                   int sh, int sm, int ss,
                   int eh, int em, int es, int st, int et, Double expected) throws ParseException {

        Bill bill = new Bill();

        Double type = bill.calculate(year, month, day, sh, sm, ss, eh, em, es, st, et);
        Assertions.assertEquals(expected, type);
    }
    @DisplayName(value = "夏令时测试用例")
    @ParameterizedTest
    @CsvFileSource(resources = "/夏令时测试用例.csv", numLinesToSkip = 1, encoding = "UTF-8")
    void fileTest3(int m, int year, int month, int day,
                   int sh, int sm, int ss,
                   int eh, int em, int es, int st, int et, int expected) throws ParseException {

        Bill bill = new Bill();
        boolean inDayLightSavingOfStart = st == 1;
        boolean inDayLightSavingOfEnd = et == 1;
        int type = bill.isEndTimeSubOrStartTimeSubOrDoNothing(inDayLightSavingOfStart,inDayLightSavingOfEnd);
        Assertions.assertEquals(expected, type);
    }
    @DisplayName(value = "小于或等于20分钟用例")
    @ParameterizedTest
    @CsvFileSource(resources = "/小于或等于20分钟用例.csv", numLinesToSkip = 1, encoding = "UTF-8")
    void testifLessMinMinute(int m, int year, int month, int day,
                             int sh, int sm, int ss,
                             int eh, int em, int es, int st, int et, int expected) throws ParseException {

        Bill bill = new Bill();
        LocalDateTime startTime = LocalDateTime.of(year, month, day, sh, sm, ss);
        LocalDateTime endTime = LocalDateTime.of(year, month, day, eh, em, es);
        LocalDateTime now = LocalDateTime.now();
        boolean inDayLightSavingOfStart = st == 1;
        boolean inDayLightSavingOfEnd = et == 1;
        long timeSpan = bill.calculateTimeSpan(startTime, inDayLightSavingOfStart, endTime, inDayLightSavingOfEnd);
        boolean type = bill.isLessOrEqualMinMinute(timeSpan);
        boolean expect = expected == 1;
        Assertions.assertEquals(expect, type);
    }
}
