package org.bill;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

public class Bill {
    static int minMinute = 20;
    static double minUnivalent = 0.05;
    static double maxUnivalent = 0.10;
    static double minFee = 1.00;

    boolean isLessOrEqualMinMinute(long timeLength){
        return timeLength <= minMinute;
    }

    int isEndTimeSubOrStartTimeSubOrDoNothing(boolean inDayLightSavingOfStartTime,boolean inDayLightSavingOfEndTime){
        if (inDayLightSavingOfEndTime && !inDayLightSavingOfStartTime) {
            return 1;
        } else if (inDayLightSavingOfStartTime && !inDayLightSavingOfEndTime) {
            return 2;
        }
        return 0;
    }

    long calculateTimeSpan(LocalDateTime startTime,
                           boolean inDayLightSavingOfStartTime,
                           LocalDateTime endTime,
                           boolean inDayLightSavingOfEndTime) {
        int result = isEndTimeSubOrStartTimeSubOrDoNothing(inDayLightSavingOfStartTime,inDayLightSavingOfEndTime);
        if (result == 1) {
            endTime = endTime.minusHours(1);
        } else if (result == 2) {
            startTime = startTime.minusHours(1);
        }
        Duration duration = Duration.between(startTime, endTime);
        long timeSpan = duration.getSeconds();
        timeSpan = timeSpan / 60 + (timeSpan % 60 == 0 ? 0 : 1);
        return timeSpan;
    }

    BigDecimal calculateFee(long timeLength) {
        if (isLessOrEqualMinMinute(timeLength)) {
            BigDecimal bigDecimal = new BigDecimal(timeLength * minUnivalent);
            return bigDecimal;
        } else {
            BigDecimal bigDecimal = new BigDecimal(minFee);
            bigDecimal = bigDecimal.add(new BigDecimal((timeLength - minMinute) * maxUnivalent));
            return bigDecimal;
        }

    }

    Double calculate(int year, int month, int day,
                     int startHour, int startMinute, int startSecond,
                     int endHour, int endMinute, int endSecond,
                     int inDayLightSavingOfStartTime, int inDayLightSavingOfEndTime) {
        LocalDateTime startTime = LocalDateTime.of(year, month, day, startHour, startMinute, startSecond);
        LocalDateTime endTime = LocalDateTime.of(year, month, day, endHour, endMinute, endSecond);
        LocalDateTime now = LocalDateTime.now();
        boolean inDayLightSavingOfStart = inDayLightSavingOfStartTime == 1;
        boolean inDayLightSavingOfEnd = inDayLightSavingOfEndTime == 1;
        long timeSpan = calculateTimeSpan(startTime, inDayLightSavingOfStart, endTime, inDayLightSavingOfEnd);
        BigDecimal money = calculateFee(timeSpan);
        return money.doubleValue();
    }
}
